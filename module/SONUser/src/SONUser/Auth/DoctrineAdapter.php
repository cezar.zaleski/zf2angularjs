<?php
/**
 * Created by IntelliJ IDEA.
 * User: familia
 * Date: 15/11/15
 * Time: 23:37
 */

namespace SONUser\Adapter;

use Doctrine\ORM\EntityManager;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;


class DoctrineAdapter implements AdapterInterface{

    protected $em;
    protected $username;
    protected $password;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If authentication cannot be performed
     */
    public function authenticate()
    {
        $repository = $this->em->getRepository('SONUser\Entity\User');
        $user = $repository->findOneBy(array('username' => $this->getUsername(), 'password' => $this->getSenha()));

        if($user){
            return new Result(Result::SUCCESS, array('OK'));
        }
        return new Result(Result::FAILURE_CREDENTIAL_INVALID, array());
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->$password;
    }

    /**
     * @param $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->$password = $password;
        return $this;
    }
}