<?php
/**
 * Created by IntelliJ IDEA.
 * User: familia
 * Date: 15/11/15
 * Time: 23:31
 */
namespace SONUser\Controller;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;

class AuthController extends AbstractActionController{

    public function indexAction()
    {
        $request = $this->getRequest();
        if($request->isPost()){
            $data = $request->getPost();

            $auth = new AuthenticationService();
            $sessionStorage = new SessionStorage();
            $auth->setStorage($sessionStorage);

            /* @var $authAdapter \SONUser\Adapter\DoctrineAdapter */
            $authAdapter = $this->getServiceLocator()->get('SONUser\Adapter\DoctrineAdapter');
            $authAdapter->setUsername($data['username'])
                ->setPassword($data['password']);
            $result = $auth->authenticate($authAdapter);
            if($result->isValid()){
                $sessionStorage->write($auth->getIdentity()['user'], null);
                return new Json(array('sucess' => true));
            }
        }
        return new Json(array('sucess' => false));

    }
}