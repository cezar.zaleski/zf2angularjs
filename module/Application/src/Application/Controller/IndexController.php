<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Application\Entity\Produto;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
//        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $repo = $em->getRepository('Application\Entity\Produto');
//        $categoria = $repo->find(1);
//        $produto = new Produto();
//        $produto->setNome('Game A')
//            ->setDescricao('O game')
//            ->setCategoria($categoria);
//
//        $em->persist($produto);
//        $em->flush();
//
//        $categorias = $repo->findAll();
//        \Zend\Debug\Debug::dump($categorias);
//        exit;

        /* @var $srvCategoria \Application\Service\Categoria */
//        $srvCategoria = $this->getServiceLocator()->get('Application\Service\Categoria');
//        $srvCategoria->delete(3);

        /* @var $srvCategoria \Application\Service\Categoria */
        $srvCategoria = $this->getServiceLocator()->get('Application\Service\Produto');
//        $srvCategoria->insert(array('nome' => 'Teste', 'descricao' => 'teste', 'categoriaId' => 2));

        $categorias = $repo->findAll();
        \Zend\Debug\Debug::dump('aqui');
        exit;
        \Zend\Debug\Debug::dump($categorias);
        exit;



        return new ViewModel(array('categorias' =>$categorias));
    }
}
