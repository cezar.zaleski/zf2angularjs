<?php
/**
 * Created by IntelliJ IDEA.
 * User: familia
 * Date: 16/11/15
 * Time: 00:54
 */

namespace SONRest\Controller;
use Zend\Mvc\Controller\AbstractRestfulController;

class CategoriaController extends AbstractRestfulController{

    public function getList()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $repo = $em->getRepository('Application\Entity\Categoria');
        return $repo->findAll();
    }

    public function get($id)
    {

    }

    public function create($data)
    {
        
    }

    public function update($id, $data)
    {
        
    }

    public function delete($id)
    {
        
    }
}